package br.com.admatic.consumidor;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class Consumidor {

	static String consumoTotal,ip,porta;	
	static long tStart, tEnd, tRes;	
	
	public static void main(String[] args) throws Exception {

		Scanner s = new Scanner(System.in);
		
	    System.out.println("Qual o IP do servidor: ");
	    ip = s.next();
	    
	    System.out.println("\nQual a porta do servidor: ");
	    porta = s.next();

	    Socket socket = new Socket(ip, Integer.parseInt(porta));

		//System.out.println("Conex�o Estabelecida");

		Thread threadEnviaComando = new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					System.out.println("\nQuantas inst�ncias voc� deseja iniciar: ");

					PrintStream saida = new PrintStream(socket.getOutputStream());

					Scanner teclado = new Scanner(System.in);

					while (teclado.hasNextLine()) {

						String linha = teclado.nextLine();

						if (linha.trim().equals("")) {
							break;
						}

						int i = 0;
						while(i < Integer.parseInt(linha)){
							tStart = System.nanoTime();
							
							saida.println("consumidor;" + i);
							i++;
						}
					}

					saida.close();
					teclado.close();
					
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});

		Thread threadRecebeResposta = new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					System.out.println("Recebendo dados do servidor");
					Scanner respostaServidor = new Scanner(
							socket.getInputStream());

					while (respostaServidor.hasNextLine()) {

						String linha = respostaServidor.nextLine();
						System.out.println(linha);
						
			    		tEnd = System.nanoTime();
			    		tRes = tEnd - tStart;	
			    		System.out.println("Demorou " + tRes + " nanosegundos para retirar do Buffer");							
					}

					respostaServidor.close();
					
				} catch (IOException e) {
					throw new RuntimeException(e);
				}

			}
		});

		threadRecebeResposta.start();
		threadEnviaComando.start();
		
		threadEnviaComando.join();
		
		System.out.println("Fechando o socket do cliente");

		socket.close();

	}

}
