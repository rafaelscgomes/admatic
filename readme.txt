==============================================================================================
=================================== DESAFIO ADMATIC ==========================================
==============================================================================================

Como combinado, teremos nesse projeto tr�s aplica��es separadas. S�o elas:

- admatic.buffer
- admatic.produtor
- admatic.consumidor

A execu��o para testes poder� ser feita da seguinte forma:

1 - Executar a classe "Servidor" do projeto "admatic-buffer". Ap�s a inicializa��o, duas perguntas ser�o feitas, sobre qual a porta que deseja usar para manter a comunica��o com os clientes e quantos n�meros podem ser adicionados. Ap�s essas respostas, o servidor estar� pronto para receber as conex�es do Produtor e Consumidor.

2 - Executar a classe "Produtor" do projeto "admatic-produtor". Ap�s a inicializa��o, tr�s perguntas ser�o feitas, sobre qual ip e porta do servidor para que o produtor se conecte e quantas inst�ncias devem ser usadas para inserir valores no Buffer.

3 - Executar a classe "Consumidor" do projeto "admatic-consumidor". Ap�s a inicializa��o, tr�s perguntas ser�o feitas, sobre qual ip e porta do servidor para que o produtor se conecte e quantas inst�ncias devem ser usadas para consumir os valores armazenados no Buffer.